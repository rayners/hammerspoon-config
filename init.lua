local mash = { 'cmd', 'alt', 'ctrl' }

hs.hotkey.bind(mash, "W",
               function()
                  hs.notify.new({title="Hammerspoon", informativeText="Hello World"}):send():release()
               end
)

hs.hotkey.bind(mash, "H",
               function()
                  hs.hints.windowHints()
               end
)

hs.hotkey.bind(mash, "Right",
               function()
                  hs.window.focusedWindow():moveOneScreenEast()
               end
)

hs.hotkey.bind(mash, "Left",
               function()
                  hs.window.focusedWindow():moveOneScreenWest()
               end
)

hs.hotkey.bind(mash, "F",
               function()
                  hs.window.focusedWindow():toggleFullScreen()
               end
)

function configReload()
   hs.reload()
   hs.notify.new({ title = "Hammerspoon", informativeText = 'Config reloaded!' }):send():release()
end

hs.hotkey.bind(mash, "R", configReload)
